const http = require("http");
const express = require("express");
const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
require("dotenv").config();

const nodemailer = require("nodemailer");
const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: process.env.GMAIL_USER,
    pass: process.env.GMAIL_PASS,
  },
});

const MongoClient = require("mongodb").MongoClient;
const uri = process.env.MONGODB_URI;
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

let collection;

client.connect((err) => {
  if (err) {
    console.log(err);
  }
  collection = client.db("jonathanlee_io").collection("contacts");

  const httpServer = http.createServer(app);

  httpServer.listen(process.env.HTTP_PORT, () => {
    console.log(`HTTP server running on port ${process.env.HTTP_PORT}`);
  });
});

app.post("/submit_contact", (req, res) => {
  const contact = {
    firstname: req.body.firstname,
    surname: req.body.surname,
    email: req.body.email,
    phone: req.body.phone,
    message: req.body.message,
  };

  try {
    collection.insertOne(contact);
  } catch (e) {
    console.log(e);
  }

  console.log("Saved to database: " + new Date());

  const mailOptions = {
    from: process.env.GMAIL_USER,
    to: process.env.TARGET_EMAIL,
    subject: "Contact <" + contact.email + "> Submitted on jonathanlee.io",
    text: JSON.stringify(contact),
  };

  transporter.sendMail(mailOptions, function (err, info) {
    if (err) {
      console.log(err);
    } else {
      console.log("E-mail sent: " + info.response);
    }
  });

  res.redirect("https://jonathanlee.io");
  res.end();
});
